import {Component} from "@angular/core";
import {PokemonService} from "../services/pokemon.service";
import {User} from "../models/pokemon.model";
import {Router} from "@angular/router";
import {LoginService} from "../services/login.service";
import {CatchPokemonService} from "../services/catch-pokemon.service";

@Component({
  selector: "app-landing-page",
  templateUrl: "./landing.page.html",
  styleUrls: ["./landing.page.css"]
})
export class LandingPage {

    public user: string = "";
    private found: boolean = false;
    private trainers: User[] = [];
    private newTrainer: User = {name: "",pokemon:[]};
    constructor(private readonly pokemonService: PokemonService,
                private readonly router: Router,
                private readonly loginService: LoginService,
                private readonly catchPokemonService: CatchPokemonService) {
      if(loginService.isLoggedIn()) {
        router.navigate(["pokedex"]);
      }
    }

  /**
   * method for login click
   * checks if the user already exists
   * creates new user if not
   * sets the user as current user
   */
    public onLoginClick() {
      this.trainers = JSON.parse(localStorage.getItem("pokemonTrainers") as string );
      if (this.trainers) {
        this.trainers.forEach((trainer: User) => {
          if (trainer.name === this.user) {
            this.found = true;
            this.catchPokemonService.initPokemon(trainer.pokemon);
            localStorage.setItem("currentTrainer", JSON.stringify(trainer));
          }
        })
      }
      else {
        this.trainers = [];
      }
      if (!this.found) {
        this.newTrainer.name = this.user;
        this.trainers.push(this.newTrainer);
        this.catchPokemonService.initPokemon([]);
        localStorage.setItem("currentTrainer", JSON.stringify(this.newTrainer));
        localStorage.setItem("pokemonTrainers", JSON.stringify(this.trainers));
      }
      this.loginService.login();
      this.router.navigate(["pokedex"])
    }
}
