export interface Pokemon {
  name: string;
  avatar: string;
  url: string;
  caught: boolean;
}

export interface APIResults {
  count: number;
  next: string; //url
  previous: string; //url
  results: Pokemon[];
}

export interface User {
  name: string;
  pokemon: Pokemon[];
}
