import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {LandingPage} from "./landing-page/landing.page";
import {HttpClientModule} from "@angular/common/http";
import {PokedexPage} from "./pokedex-page/pokedex.page";
import {TrainerPage} from "./trainer-page/trainer.page";
import {PokemonListItemComponent} from "./pokemon-list-item/pokemon-list-item.component";
import {RouterModule} from "@angular/router";
import {AppRoutingModule} from "./app-routing.module";
import {FormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    AppComponent,
    LandingPage,
    PokedexPage,
    TrainerPage,
    PokemonListItemComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
