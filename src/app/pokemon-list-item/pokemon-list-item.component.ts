import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Pokemon} from "../models/pokemon.model";
import {CatchPokemonService} from "../services/catch-pokemon.service";

@Component({
  selector: "app-pokemon-list-item",
  templateUrl: "pokemon-list-item.component.html",
  styleUrls: ["pokemon-list-item.component.css"]
})

export class PokemonListItemComponent {
  @Input() pokemonItem: Pokemon | undefined;
  @Output() clicked: EventEmitter<Pokemon> = new EventEmitter();

  constructor() {
  }

  /**
   * method for when an pokemon is clicked
   * marks the pokemon as caught and emits the event to the parent
   */
  public onPokemonClicked(): void {
    if (this.pokemonItem) {
      this.pokemonItem.caught = true;
    }
    this.clicked.emit(this.pokemonItem);
  }
}
