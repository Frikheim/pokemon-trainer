import {Injectable} from "@angular/core";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {APIResults, Pokemon} from "../models/pokemon.model";
import {CatchPokemonService} from "./catch-pokemon.service";

@Injectable({
  providedIn: "root"
})
export class PokemonService {
  private pokemonList: Pokemon[] = [];
  private error: string = "";

  constructor(private readonly http: HttpClient,
              private readonly catchPokemonService: CatchPokemonService) {
  }

  /**
   * method for fetching pokemon
   * cuurently fetches first 20, can fetch more
   */
  public fetchPokemon(): void {
    this.http.get<APIResults>("https://pokeapi.co/api/v2/pokemon")
      .subscribe((aPIResults: APIResults) => {
        this.pokemonList = aPIResults.results;
        this.formatPokemon();
      }, (error: HttpErrorResponse) => {
        this.error = error.message;
      });
  }

  public getPokemon() {
    return this.pokemonList;
  }

  /**
   * method for formating pokemon
   * creates the avatar url and checks if the pokemon has been caught by the user
   */
  public formatPokemon(): void {
    this.pokemonList.forEach(pokemon => {
      let detailsArray = pokemon.url.split("/");
      pokemon.avatar = `assets/pokemon-sprites/sprites/pokemon/${detailsArray[detailsArray.length-2]}.png`;
      this.catchPokemonService.pokemon().forEach(caughtPokemon => {
        if (caughtPokemon.name === pokemon.name) {
          pokemon.caught = true;
        }
      });
    });
  }
}
