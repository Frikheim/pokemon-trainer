import {Injectable} from "@angular/core";
import {User} from "../models/pokemon.model";
import {CatchPokemonService} from "./catch-pokemon.service";

@Injectable({
  providedIn: "root"
})
export class LoginService {
  private loggedIn:boolean;
  private readonly initialUser: User;


  constructor(private readonly catchPokemonService: CatchPokemonService) {
    //initialises the app if the user is already loggedin
    this.initialUser = JSON.parse(localStorage.getItem("currentTrainer") as string );
    if (this.initialUser) {
      catchPokemonService.initPokemon(this.initialUser.pokemon);
      this.loggedIn = true;
    }
    else {
      this.loggedIn = false;
    }
  }

  /**
   * method for setting logged in to true
   */
  public login(): void {
    this.loggedIn = true;
  }

  /**
   * method for setting logged in to true
   */
  public logout(): void {
    this.loggedIn = false;
    localStorage.removeItem("currentTrainer");
  }

  /**
   * method for getting login state
   */
  public isLoggedIn(): boolean {
    return this.loggedIn
  }
}
