import {Injectable} from "@angular/core";
import {Pokemon, User} from "../models/pokemon.model";

@Injectable({
  providedIn: "root"
})
export class CatchPokemonService {
  private catchedPokemon: Pokemon[] = [];
  private currentTrainer: User | undefined;
  private trainers: User[] = [];

  /**
   * method for catching pokemon
   * updates the list of caught pokemons and updates the user in currentTrainer and trainers
   */
  public catchPokemon(pokemon: Pokemon) {
    this.catchedPokemon.push(pokemon);
    this.trainers = JSON.parse(localStorage.getItem("pokemonTrainers") as string );
    this.currentTrainer = JSON.parse(localStorage.getItem("currentTrainer") as string );
    this.currentTrainer!.pokemon = this.catchedPokemon;
    localStorage.setItem("currentTrainer", JSON.stringify(this.currentTrainer));
    this.trainers.forEach((trainer: User) => {
      if (trainer.name === this.currentTrainer!.name) {
        trainer.pokemon = this.catchedPokemon;
        localStorage.setItem("pokemonTrainers", JSON.stringify(this.trainers));
      }
    })
  }

  public pokemon(): Pokemon[] {
    return this.catchedPokemon;
  }
  /**
   * method for initialising the pokemon array
   */
  public initPokemon(pokemon: Pokemon[]): void {
    this.catchedPokemon = pokemon;
  }
}
