import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {LandingPage} from "./landing-page/landing.page";
import {TrainerPage} from "./trainer-page/trainer.page";
import {PokedexPage} from "./pokedex-page/pokedex.page";

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "/login"
  },
  {
  path: "login",
  component: LandingPage
  },
  {
    path: "trainer",
    component: TrainerPage
  },
  {
    path: "pokedex",
    component: PokedexPage
  }
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}
