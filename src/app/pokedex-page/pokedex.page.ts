import {Component, OnInit} from "@angular/core";
import {PokemonService} from "../services/pokemon.service";
import {Pokemon} from "../models/pokemon.model";
import {CatchPokemonService} from "../services/catch-pokemon.service";
import {LoginService} from "../services/login.service";
import {Router} from "@angular/router";

@Component({
  selector: "app-pokedex-page",
  templateUrl: "./pokedex.page.html",
  styleUrls: ["./pokedex.page.css"]
})
export class PokedexPage{


  constructor(private  readonly pokemonService: PokemonService,
              private  readonly catchPokemonService: CatchPokemonService,
              private readonly loginService: LoginService,
              private readonly router: Router) {
    //fetches pokemon
    this.pokemonService.fetchPokemon();
    //checks if the user is loggedin
    if (!loginService.isLoggedIn()) {
      router.navigate(["login"])
    }
  }

  get pokemonList(): Pokemon[] {
    return this.pokemonService.getPokemon();
  }

  /**
   * method for cathing pokemon
   * handles the catch event from the child using the catch pokemon service
   */
  public handlePokemonClicked(pokemon: Pokemon): void {
    this.catchPokemonService.catchPokemon(pokemon);
  }
}
