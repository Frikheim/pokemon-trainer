import {Component} from "@angular/core";
import {Pokemon, User} from "../models/pokemon.model";
import {CatchPokemonService} from "../services/catch-pokemon.service";
import {Router} from "@angular/router";
import {LoginService} from "../services/login.service";

@Component({
  selector: "app-trainer-page",
  templateUrl: "./trainer.page.html",
  styleUrls: ["./trainer.page.css"]
})
export class TrainerPage {

  public trainer: User;

  constructor(private  readonly catchPokemonService: CatchPokemonService,private readonly router: Router, private readonly loginService: LoginService) {
    //checks if the user is logged in
    if (!this.loginService.isLoggedIn()) {
      this.router.navigate(['login']);
    }
    //initialises current trainer
    this.trainer = JSON.parse(localStorage.getItem("currentTrainer") as string );
  }

  /**
   *method for logging out the user when the logoutbutton is clicked
   * forwards the user to the login page
   **/
  public onLogoutClick() {
    this.loginService.logout();
    this.router.navigate(['login']);
  }

  get catchedPokemon(): Pokemon[] {
    return this.catchPokemonService.pokemon();
  }
}
